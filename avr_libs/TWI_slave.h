/*
 *
 * Name: USI TWI Slave driver - I2C/TWI-EEPROM
 * Version: 1.3  - Stable
 * autor: Martin Junghans	jtronics@gmx.de
 * page: www.jtronics.de
 * License: GNU General Public License
 *
 * Created from Atmel source files for Application Note AVR312:
 * Using the USI Module as an I2C slave like an I2C-EEPROM.
 *
 * LICENSE: Copyright (C) 2010 Marin Junghans
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details
 *
 * 16.11.2010 - reworked by Markus Schatzl
 *
 * 02.02.2013 - reworked by Willi Thiel
 *
 * 30.06.2018 - adjusted for this project by Max Becker
 */

#ifndef _I2CSLAVE_H_
#define _I2CSLAVE_H_

/**
 * Initialize slave
 *
 * @param address The address of the I2C slave device
 */
void i2c_init(uint8_t address);

#define buffer_size 16 /*!< I2C buffer size (in bytes (2..254)) */
extern volatile uint8_t i2c_buffer[buffer_size]; /*!< I2C buffer */
extern volatile uint8_t buffer_adr; /*!< Virtual buffer address register */

#if    (buffer_size > 254)
#error Buffer to big! 254 bytes max.

#elif    (buffer_size < 2)
#error Buffer to small! 2 bytes min.
#endif


#define DDR_USI             DDRA
#define PORT_USI            PORTA
#define PIN_USI             PINA
#define PORT_USI_SDA        PA0
#define PORT_USI_SCL        PA2
#define PIN_USI_SDA         PINA0
#define PIN_USI_SCL         PINA2
#define USI_START_COND_INT  USISIF
#define USI_START_VECTOR    USI_START_vect
#define USI_OVERFLOW_VECTOR USI_OVF_vect

extern void (*i2c_onReceivePtr)(uint8_t addr, uint8_t data);

extern uint8_t (*i2c_onRequestPtr)(uint8_t addr);

#endif  // ifndef I2CSLAVE_H_
