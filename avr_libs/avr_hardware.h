
#ifndef LIB_AVR_HARDWARE_H
#define LIB_AVR_HARDWARE_H

#ifdef ATTINY261
#define ATTINYx61
#endif

#ifdef ATTINY461
#define ATTINYx61
#endif

#ifdef ATTINY861
#define ATTINYx61
#endif

#ifdef ATTINYx61

#define SETUP_IR_OUT() ({ \
    /*Reset*/ \
    TCCR1D = 0; \
    TCCR1C = 0; \
    TCCR1B = 0; \
    \
    /*Prepare and start PWM*/\
    /*Do not connect to output right away TCCR1C = ( 1 << COM1D1 ); //Clear on upwards match - Set on downwards match  //Set to zero to disable */\
    TCCR1D = ( 1 << WGM10 ); /* Phase and Frequency corrent PWM //Page 118 */ \
    TCCR1C |= (1 << PWM1D); /* Enable PWM with comparator A */ \
    TCCR1B = (1 << CS10); /* Enable timer clock with prescaling of 1 */ \
    /* Clock speed: 8MHz=8MHz */ \
    /* PWM Frequency = CLK / OCR1C / 2 */ \
    /* => OCR1C = CLK /2 /38k  */\
    /* Duty cycle: 30% -> OCR1D = 432*/\
    OCR1D = 32; /* Set comparator value // CPU Speed 8MHz 8000000/64 = 15625*8 */ \
    OCR1C = 105; /* Set top limit */\
    \
    /* Prepare output port */\
    DDRB |= (1<<PB5);  /* Enable port output */\
    PORTB &= ~(1 <<PB5);  /* Set output off */\
})

#define UNSETUP_IR_OUT() ({ \
    TCCR1D = 0; \
    TCCR1C = 0; \
    TCCR1B = 0; \
    DDRB &= ~(1<<PB5);  /*sdfg*/\
})

#define ENABLE_IR_OUT (TCCR1C |= ( 1 << COM1D1 ))
#define DISABLE_IR_OUT (TCCR1C &= ~( 1 << COM1D1 ))

/* RECEIVE SETUP
 * - Enable TIMER1 without prescale (CS10). This results in a counter increase every 1us.
 * - Set top limit for counter to our desired 50(us) (OCR1C)
 * - Reset any other related registers
 *
 * Interrupts are enabled by setting OCIE1A in TIMSK
 */
#define SETUP_IR_IN_TIMER() ({\
  TCCR1A = 0;\
  TCCR1B = (1 << CS12); /* by 8 prescale*/\
  OCR1C = USECPERTICK ;\
  TCNT1 = 0;\
})
#endif
#define ENABLE_IR_IN_INTR   (TIMSK |= 1 << OCIE1A)
#define DISABLE_IR_IN_INTR (TIMSK &= ~(1 << OCIE1A))
#define IR_IN_TIMER_INTR_NAME TIMER1_COMPA_vect


//Approximately possible r80ates: 500 - 62.000
#define SETUP_SOUND_OUT(rate) ({\
  DDRB |= (1<<PB3); /* Enable port output and set it to off for now*/\
  PORTB &= ~(1<<PB3);\
  /* Setup Timer 1 for pulse width modulation with max frequency (internal clock, fast PWM, no prescaler*/ \
  TCCR1B = (1 << CS10);\
  TCCR1A = (1 << PWM1B) | (1 << COM1B1);\
  /* Setup Timer 2 to send a sample every interrupt */\
  TCCR0A = (1 << CTC0);\
  TCCR0B = (1 << CS01) | (1 << CS00);\
  OCR0A = F_CPU / 64 / rate;\
  TIMSK = (1 << OCIE0A);\
})

#define SET_SAMPLE_RATE(rate) ({\
  OCR0A = F_CPU / 64 / rate;\
})

#define SOUND_OUT_PWM_REG OCR1B

#define DISABLE_SOUND_OUT() ({\
  TIMSK &= ~(1 << OCIE0A);\
  TCCR0A = 0;\
  TCCR1B &= ~(1 << CS10);\
  TCCR1A = 0;\
  PORTB &= ~(1 <<PB3);\
  DDRB &= ~(1 <<PB3);\
})


#endif //LIB_AVR_HARDWARE_H
