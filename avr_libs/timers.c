

#include "timers.h"

/* Some devices might be running with a clk frequency as low as 1MHz, which results in a minimum possible timing resolution of 1us
 * However this is not really required. Possible prescalers 8,64,256.
 * Choosing 8 will give us a resolution of 8 (or lower for higher clks)
 */

// The timer has a 8 bit register so it overflows after 256 ticks
// One tick is 1/8 (prescale) us
#define MICROSECONDS_PER_TIMER0_OVERFLOW (clockCyclesToMicroseconds(8 * 256))

// During one "overflow cycle" we get some full ms and a few fractals
#define MILLIS_INC (MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)
#define FRACT_INC ((MICROSECONDS_PER_TIMER0_OVERFLOW %1000 ))

volatile uint32_t timer0_overflow_count = 0;
volatile uint32_t timer0_millis = 0;
static uint16_t timer0_fract = 0;

ISR(TIMER0_OVF_vect){
    // copy these to local variables so they can be stored in registers
    // (volatile variables must be read from memory on every access)
    uint32_t m = timer0_millis;
    uint16_t f = timer0_fract;
    m += MILLIS_INC;
    f += FRACT_INC;
    if (f >= 1000) {
        f -= 1000;
        m += 1;
    }

    timer0_fract = f;
    timer0_millis = m;
    timer0_overflow_count++;
}


uint32_t millis(void){
    uint32_t m;
    uint8_t oldSREG = SREG; //Store old main control register (including the global interrupt enable bit)

    // disable interrupts while we read timer0_millis or we might get an
    // inconsistent value (e.g. in the middle of a write to timer0_millis)
    cli();
    m = timer0_millis;
    SREG = oldSREG; //Restore the main control register (and thereby enable the interrupts again

    return m;
}

uint32_t micros(void){
    uint32_t m;
    uint8_t oldSREG = SREG;
    uint8_t t;

    cli();
    m = timer0_overflow_count;

    t = TCNT0L;

    if ((TIFR & _BV(TOV0)) && (t < 255))
        m++;

    SREG = oldSREG;

    return ((m << 8) + t) * (8 / clockCyclesPerMicrosecond());
}


void delay(uint32_t ms)
{
    uint32_t start = micros();

    while (ms > 0) {
        while ( ms > 0 && (micros() - start) >= 1000) {
            ms--;
            start += 1000;
        }
    }
}


void delay_usec(uint32_t uSecs) {
    if (uSecs > 4) {
        uint32_t start = micros();
        uint32_t endMicros = start + uSecs - 4;
        if (endMicros < start) { // Check if overflow
            while ( micros() > start ) {} // wait until overflow
        }
        while ( micros() < endMicros ) {} // normal wait
    }
    //else {
    //  __asm__("nop\n\t"); // must have or compiler optimizes out
    //}
}


void initTimers(void)
{
    // this needs to be called before setup() or some functions won't
    // work there
    sei();

    TCCR0B = (1 << CS01); //Reset timer register and select prescaler 8

    TIMSK |= (1 << TOIE0); //Enable overflow interrupt
}

void pauseTimers(void) {
    TCCR0B = 0;
    TIMSK &= ~(1 << TOIE0);
}
