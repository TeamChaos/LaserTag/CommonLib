

#include <stdint.h>
#include <avr/interrupt.h>

#ifndef LIB_TIMERS_H
#define LIB_TIMERS_H

#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ( (a) / clockCyclesPerMicrosecond() )

extern uint32_t millis(void);
extern uint32_t micros(void);
extern void delay_usec(uint32_t uSecs);
extern void initTimers(void);

extern void pauseTimers(void);
extern void delay(uint32_t ms);

#endif //LIB_TIMERS_H

