#ifndef DETECTOR_CONTROL_PROTOCOL
#define DETECTOR_CONTROL_PROTOCOL

#define DETECTOR_TWI_ADDRESS 0x10

/*
 * The transmitter can be paused by setting its state to STATUS_PAUSED.
 * If the transmit pin is high (STATUS_TRANSMIT) you are supposed to read all DATA bytes by starting with I2C_GET_ALL and then continuing until I2C_GET_ALL2 by acknowledging with ACKs (and then terminating with an NACK).
 * Once the I2C_GET_ALL2 byte is read the detector automatically reset to STATUS_DETECTING.
 *
 * The data byte I2C_ERROR_BYTE (0xFF= is defined as error byte and is therefore not available as valid data value.
 * If the detector responds with this byte something is wrong (e.g. trying to retrieve data while in STATUS_DETECTING).
 */

//Get/Set detector status
#define I2C_STATUS_GET 0x00
#define I2C_STATUS_SET 0x00

//Three available states
#define STATUS_DETECTING 0x00u
#define STATUS_PAUSED 0x01
#define STATUS_TRANSMIT 0x02 //The transmit is supposed to be enabled while the detector is in this state

//These "addresses" are used to retrieve the received and decoded IR signal. Only read them while detector_status is STATUS_TRANSMIT otherwise the returned value is I2C_ERROR_BYTE
#define I2C_GET_ALL 0x01 //Returns I2C_ALL_OK
#define I2C_ALL_OK 0x01
#define I2C_ID_GET 0x02 //Stores decoded player flag (f) and id (i) and the byte parity bit (p) 0bp00fiiii
#define I2C_DATA_GET 0x03 //Returns decoded data(d) and the byte parity bit (p) 0bp00ddddd
#define I2C_D_STATUS_ONE_GET 0x04 //One bit corresponds to one receiver. 1 means it received a message - LSB: Receiver 1, 2nd-MSB: Receiver 7, MSB: 0
#define I2C_D_STATUS_TWO_GET 0x05 //One bit corresponds to one receiver. 1 means it received a message - LSB: Receiver 8
#define I2C_GET_ALL2 0x06  //Returns I2C_ALL_OK and resets status to STATUS_DETECTING


#define I2C_ERRORS_GET 0x10
#define I2C_ERRORS_I2C_GET 0x11


//Debug read
//8byte wide string
#define I2C_READ_VERSION1 0x40
#define I2C_READ_VERSION2 0x41
#define I2C_READ_VERSION3 0x42
#define I2C_READ_VERSION4 0x43
#define I2C_READ_VERSION5 0x44
#define I2C_READ_VERSION6 0x45
#define I2C_READ_VERSION7 0x46
#define I2C_READ_VERSION8 0x47


#define I2C_ERROR_BYTE 0xFF

#endif
