
#ifndef IR_PROTOCOL_H
#define IR_PROTOCOL_H

#define START_MARK 8000UL
#define START_SPACE 4000UL
#define ONE_SPACE 1600UL
#define ZERO_SPACE 550UL
#define BIT_MARK 600UL

//The following values are used for detection. They are similar to the values above but divided by 50 (USECSPERTICK) and a tolerance (3/4; 5/4) is included. They might have been adjusted based on testing.
//Precalculating these reduces run time (but might increase program size slightly.
//In addition the precalculation done by the compiler produces some strange results. Or maybe I am just too stupid
#define START_MARK_LOW_T 90
#define START_MARK_HIGH_T 220
#define START_SPACE_LOW_T 50
#define START_SPACE_HIGH_T 120
#define ONE_SPACE_LOW_T 20
#define ONE_SPACE_HIGH_T 40
#define ZERO_SPACE_LOW_T 7
#define ZERO_SPACE_HIGH_T 19
#define BIT_MARK_LOW_T 7
#define BIT_MARK_HIGH_T 20


#endif //IR_PROTOCOL_H
