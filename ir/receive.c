

#include <avr/interrupt.h>
#include "receive.h"
#include "../avr_libs/timers.h"


void reset() {
    irparams.rcvstate = STATE_IDLE;
    irparams.rawlen = 0;
    active_pins_one = 0;
    active_pins_two = 0;

}

void setupIRIn(void) {
    //Init state machine
    reset();

    cli();//Disable interrupts temporarily

    SETUP_IR_IN_TIMER();

    sei();//Enable interrupts again


}

ISR(IR_IN_TIMER_INTR_NAME) {

    uint8_t irdata1 = RECEIVER_BYTE_ONE;
    uint8_t irdata2 = RECEIVER_BYTE_TWO;

    //If any receiver should be low (MARK), the corresponding receiver bit should be 1. We consider this as a MARK
    bool irdata = irdata1 || irdata2;


    //Remember any receiver bit active during any mark. These two bytes are only reset after a complete transmission, so at the end we should have a one for every receiver which was active for at least one tick during the transmission
    if (irdata1) {
        active_pins_one |= irdata1;
    }
    if (irdata2) {
        active_pins_two |= irdata2;
    }

    //Increase tick counter
    irparams.timer++;

    if (irparams.rawlen >= RAWBUF) irparams.rcvstate = STATE_OVERFLOW ; // Buffer overflow


    //Switch: about 200 bytes
    switch (irparams.rcvstate) {
        //......................................................................
        case STATE_IDLE: // In the middle of a gap
            if (irdata) {
                    // Gap just ended; Record duration; Start recording transmission
                    irparams.overflow = false;
                    irparams.rawlen = 0;
                    irparams.rawbuf[irparams.rawlen++] = irparams.timer;
                    irparams.timer = 0;
                    irparams.rcvstate = STATE_MARK;


            }
            break;
            //......................................................................
        case STATE_MARK:  // Timing Mark
            if (!irdata) {   // Mark ended; Record time
                irparams.rawbuf[irparams.rawlen++] = irparams.timer;
                irparams.timer = 0;
                irparams.rcvstate = STATE_SPACE;
            }
            break;
            //......................................................................
        case STATE_SPACE:  // Timing Space
            if (irdata) {  // Space just ended; Record time
                irparams.rawbuf[irparams.rawlen++] = irparams.timer;
                irparams.timer = 0;
                irparams.rcvstate = STATE_MARK;

            } else if (irparams.timer > GAP_TICKS) {  // Space
                // A long Space, indicates gap between codes
                // Flag the current code as ready for processing
                // Switch to STOP
                // Don't reset timer; keep counting Space width
                irparams.rcvstate = STATE_STOP;
            }
            break;
            //......................................................................
        case STATE_STOP:  // Waiting; Measuring Gap
            if (irdata) irparams.timer = 0;  // Reset gap timer
            break;
            //......................................................................
        case STATE_OVERFLOW:  // Flag up a read overflow; Stop the State Machine
            irparams.overflow = true;
            irparams.rcvstate = STATE_STOP;
            break;
    }


}


int MATCH(uint8_t measured_ticks, uint8_t min, uint8_t max) {
    return (measured_ticks >= min) && (measured_ticks <= max);
}



//200 byte

bool decodeData(decode_results *results) {

    //IR interrupts have to be disabled here

    if (irparams.rawlen < 2 * 11 +
                          3) //We expect 11 bits + start mark and space + end bit mark. Since the pre signal gap is counted too, we probably want one more
        return false;


    uint16_t data = 0; //Store binary result. 16 bit variable (uint16_t) is bad for performance but makes stuff simpler
    int offset = 1; //Buffer index. Skip 0 as it contains the gap


    if (!MATCH(irparams.rawbuf[offset++], START_MARK_LOW_T, START_MARK_HIGH_T)) return false;



    if (!MATCH(irparams.rawbuf[offset++], START_SPACE_LOW_T, START_SPACE_HIGH_T)) return false;



    //Decode bits.
    for (int i = 0; i < 11; i++) {


        if (!MATCH(irparams.rawbuf[offset++], BIT_MARK_LOW_T, BIT_MARK_HIGH_T)) return false;



        uint8_t space = irparams.rawbuf[offset]; //This is the timing determining if we have a 1 or 0


        if (MATCH(space, ONE_SPACE_LOW_T, ONE_SPACE_HIGH_T)) {
            data = (data << 1) | 1;

        } else if (MATCH(space, ZERO_SPACE_LOW_T, ZERO_SPACE_HIGH_T)) {
            data = (data << 1) | 0;

        } else {
            return false;
        }
        offset++;
    }


    if (!MATCH(irparams.rawbuf[offset++], BIT_MARK_LOW_T, BIT_MARK_HIGH_T)) return false;

    results->player = (data & 0b10000000000) != 0; //TODO switch to built in parity function
    results->id = (data & 0b01111000000) >> 6;
    results->data = (data & 0b00000111110) >> 1;
    uint16_t received_parity = (data & 0b1);
    uint16_t parity = data & 0b11111111110;
    parity ^= (parity >> 8);
    parity ^= (parity >> 4);
    parity ^= (parity >> 2);
    parity ^= (parity >> 1);
    results->parity_ok = received_parity == (parity & 0b1);
    return true;
}


void debugBuffer(volatile irparams_t *params);

uint8_t decode(decode_results *results) {
    if (irparams.rcvstate != STATE_STOP) return 0;


    if (irparams.overflow) {
        return 3;
    }


    //debugBuffer();
    DISABLE_IR_IN_INTR;
    uint8_t result=decodeData(results) ? 1 : 2;
    ENABLE_IR_IN_INTR;
    return result;

}


//DEBUG Methods


//Display buffer values (timings)

void debugBuffer(volatile irparams_t *params) {
    if (params->rawlen < 2 * 11 + 3) {
        return;
    }
    delay(1000);


    int offset = 1;


    for (int i = 0; i < 13; i++) {
        unsigned int temp = params->rawbuf[offset++];
        if (temp > 110 && temp < 130) {
            PORTA |= (1 << PA2);  //Set output off
            delay(1000);
            PORTA &= ~(1 << PA2);  //Set output off
        } else if (temp < 100) {
            PORTA |= (1 << PA0);  //Set output off
            delay(temp * 100);
            PORTA &= ~(1 << PA0);  //Set output off
        } else if (temp < 1000) {
            PORTA |= (1 << PA1);  //Set output off
            delay(temp * 20);
            PORTA &= ~(1 << PA1);  //Set output off
        } else {

        }
        delay(1000);
    }

}

//Attempt manual decoding of a defined data
/*
void good(){
    PORTA |= (1 << PA1);  //Set output off
    delay(500);
    PORTA &= ~(1 << PA1);  //Set output off
    delay(500);

}

void bad(){
    PORTA |= (1 << PA2);  //Set output off
    delay(500);
    PORTA &= ~(1 << PA2);  //Set output off
    delay(500);
}

void debugManual(){
    if(irparams.rawlen < 2 * 11 + 3){
        return ;
    }
    delay(1000);

    int offset=1;
    uint8_t  temp = irparams.rawbuf[offset++];
    if( temp > 90 && temp < 140){ //90 160
        good();
    }
    else {
        bad();
    }
    temp = irparams.rawbuf[offset++];
    if( temp > 50 && temp < 80){ //50 100
        good();
    }
    else {
        bad();
    }
    temp = irparams.rawbuf[offset++];
    if( temp > 7 && temp < 13){ // 7 15
        good();
    }
    else {
        bad();
    }
    temp = irparams.rawbuf[offset++];
    if( temp > 20 && temp < 30){ //20 40
        good();
    }
    else {
        bad();
    }
    temp = irparams.rawbuf[offset++];
    if( temp > 7 && temp < 13){ //7 15
        good();
    }
    else {
        bad();
    }
    temp = irparams.rawbuf[offset++];
    if( temp > 7 && temp < 12){ //7 14
        good();
    }
    else {
        bad();
    }
    temp = irparams.rawbuf[offset++];
    if( temp > 7 && temp < 13){ //7 15
        good();
    }
    else {
        bad();
    }
}
*/
