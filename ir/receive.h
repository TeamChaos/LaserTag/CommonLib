
#ifndef LIB_IR_RECEIVE_H
#define LIB_IR_RECEIVE_H

/**
 * You need to provide a library configuration file ("lib_config.h") in the CommonLib parent directory
 * You need to define two macros "RECEIVER_BYTE_ONE" and "RECEIVER_BYTE_TWO". Each bit should correspond to one receiver. Unused bits should always be 0. The others should be high if the receiver is low (MARK).
 */
#include "../../lib_config.h"
#include <stdbool.h>
#include <stdint.h>
#include "../avr_libs/avr_hardware.h"
#include "protocol.h"


/**
 *  Store decoded results
 */
typedef struct {
    bool player;
    uint8_t id;
    uint8_t data;
    bool parity_ok;
} decode_results;

//Tick/Check input every 50 us - If this is changed the precalculated values in protocol.h have to be changed too
#define USECPERTICK 50

//Input state buffer size - has to fit the number of marks/gaps the transmitter sends
#define RAWBUF 28

typedef struct {
    // The fields are ordered to reduce memory over caused by struct-padding
    uint8_t rcvstate;        // State Machine state
    uint8_t rawlen;          // counter of entries in rawbuf
    uint8_t timer;           // State timer, counts 50uS ticks.
    uint8_t rawbuf[RAWBUF];  // raw data
    uint8_t overflow;        // Raw buffer overflow occurred
} irparams_t;


//Receiver state machine
#define STATE_IDLE      2
#define STATE_MARK      3
#define STATE_SPACE     4
#define STATE_STOP      5
#define STATE_OVERFLOW 6

// Allow all parts of the code access to the ISR data
// NB. The data can be changed by the ISR at any time, even mid-function
// Therefore we declare it as "volatile" to stop the compiler/CPU caching it
volatile irparams_t irparams;

volatile uint8_t active_pins_one;
volatile uint8_t active_pins_two;


//Require at least 5ms between transmissions
#define GAP_TICKS (5000/USECPERTICK)

/**
 * Check if the state machine has stopped and if so decode the data
 * @param results If success this is filled with the data
 * @return 0 if not stopped, 1 if success, 2 if failed to decode and 3 if overflow
 */
uint8_t decode(decode_results *results);

/**
 * Setup the receiver
 */
void setupIRIn(void);

/**
 * Reset state machine to IDLE, reset active receiver bits, ready to receive new transmissions
 */
void reset();

#endif //LIB_IR_RECEIVE_H
