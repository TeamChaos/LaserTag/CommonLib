
#include "send.h"



//Predeclare
void space(uint32_t uSecs);
void mark(uint32_t uSecs);


void sendIRMessage(bool player, uint8_t player_id, uint8_t data){


    uint16_t raw = (player ? 1U : 0) << 10;
    raw |= (player_id & 0b00001111) << 6;
    raw |= (data & 0b00011111) << 1;
    uint16_t parity = raw ^ (raw >> 8);
    parity ^= parity >> 4;
    parity ^= parity >> 2;
    parity ^= parity >> 1;
    raw |= (parity) & 0b00000001;

    SETUP_IR_OUT();

    mark(START_MARK);
    space(START_SPACE);
    mark(BIT_MARK);

    for(uint16_t mask = 1U << (10); mask; mask >>= 1){
        if(raw & mask){
            space(ONE_SPACE);
            mark(BIT_MARK);
        } else {
            space(ZERO_SPACE);
            mark(BIT_MARK);
        }
    }

    UNSETUP_IR_OUT();
}




void mark(uint32_t uSecs){
    ENABLE_IR_OUT;
    delay_usec(uSecs);

}

void space(uint32_t uSecs){
    DISABLE_IR_OUT;
    delay_usec(uSecs);
}
