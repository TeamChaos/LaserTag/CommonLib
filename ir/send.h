//
// Created by max on 09.06.18.
//
#include <stdint.h>
#include <stdbool.h>
#include "protocol.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "../avr_libs/avr_hardware.h"
#include "../avr_libs/timers.h"

#ifndef LIB_IR_SEND_H
#define LIB_IR_SEND_H

/**
 * Sends an IR message
 * Enables IR out in the beginning and disables it in the end
 * Encodes the data properly and adds a parity bit
 *
 * Only the 4 LSB of player_id and the 5 LSB of data are transmitted
 **/
void sendIRMessage(bool player, uint8_t player_id, uint8_t data);

#endif //LIB_IR_SEND_H
