
#ifndef WEAPON_PROTOCOL_H
#define WEAPON_PROTOCOL_H

#define WEAPON_I2C_ADDRESS 0x11


//Main control (Write/Read)
#define I2C_ENABLE 0x01 //Also enables/disables LEDs

//Control (Write)
#define I2C_ADD_MAG 0x41 //Add magazines
#define I2C_KILL_CONFIRM 0x42 //Turns on the kill confirm LED for n * 40ms

//Write
#define I2C_SETUP1 0x10 //Resets and disables weapon prior to configuration. Data is ignored
#define I2C_WEAPON_ID 0x11
#define I2C_DAMAGE 0x12
#define I2C_FIRE_DELAY 0x13  //n * 40ms -> allows up to 10s - Lowest reasonable value is currently around 8 as shooting sound takes 320ms
#define I2C_FIRE_MODE 0x14  //1: Semiauto, 0: Auto
#define I2C_MAG_SIZE 0x15
#define I2C_RELOAD_TIME 0x16  //n*80ms -> allows up to 20s
#define I2C_INITIAL_MAGS 0x17
#define I2C_SETUP2 0x18 //Finalizes setup. Loads magazine. Data is ignored. Does NOT enable weapon

//Read Write
#define I2C_MAGS 0x21 //Set magazines


//Read
#define I2C_STATUS 0x30 //Should return 0xFF if everything is ok
#define I2C_SHOTS_FIRED_LSB 0x31
#define I2C_SHOTS_FIRED_MSB 0x32
#define I2C_AMMUNITION 0x33 //In current magazine


//Config read
#define I2C_READ_ID 0xE0 //Return 1byte weapon id. Should be unique for each weapon

//Debug read
//8byte wide string
#define I2C_READ_VERSION1 0xF0
#define I2C_READ_VERSION2 0xF1
#define I2C_READ_VERSION3 0xF2
#define I2C_READ_VERSION4 0xF3
#define I2C_READ_VERSION5 0xF4
#define I2C_READ_VERSION6 0xF5
#define I2C_READ_VERSION7 0xF6
#define I2C_READ_VERSION8 0xF7


#endif //WEAPON_PROTOCOL_H
